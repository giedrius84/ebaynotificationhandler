﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Xml.Linq;

namespace EbayItemRevised
{
    /// <summary>
    /// Processess eBayNotifications
    /// </summary>
    public class EbayNotificationHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                Stream stream = context.Request.InputStream;
                ProcessXmlRequest(XDocument.Load(stream));
            }
            catch (Exception ex)
            {
                string errorMessage = $"Exception occured: {ex}.";
                File.AppendAllText(HttpContext.Current.Server.MapPath("exceptionLog.txt"), errorMessage);
            }

            //return OK response code
            context.Response.StatusCode = 200;
        }

        private void ProcessXmlRequest(XDocument doc)
        {
            string itemId = doc.Descendants().
                FirstOrDefault(node => node.Name.LocalName == "ItemID")?.Value;

            string fileName = DateTime.Now.ToString("yyyyMMddTHHmmss") + $"-{itemId}";

            EnsureNotificationFolder();

            doc.Save(HttpContext.Current.Server.MapPath($"{Settings.NotificationFolder}/{fileName}.xml"));

            ValidateItemId(itemId, fileName);
            ValidateSignature(doc, fileName);
        }

        private void EnsureNotificationFolder()
        {
            string directoryPath = HttpContext.Current.Server.MapPath($"{Settings.NotificationFolder}");
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }

        private static void ValidateSignature(XDocument doc, string fileName)
        {
            if (!IsSignatureValid(doc))
            {
                string errorMessage = $"Signature validation failed for: {fileName}.";
                File.AppendAllLines(HttpContext.Current.Server.MapPath($"{Settings.NotificationFolder}/errors.txt"), new[] { errorMessage });
            }
        }

        private static void ValidateItemId(string itemId, string fileName)
        {
            if (string.IsNullOrEmpty(itemId))
            {
                string errorMessage = $"ItemId was not found in: {fileName}.{Environment.NewLine}";
                File.AppendAllLines(HttpContext.Current.Server.MapPath($"{Settings.NotificationFolder}/errors.txt"), new[] { errorMessage });
            }
        }

        private static bool IsSignatureValid(XDocument doc)
        {
            string signature = doc.Descendants().FirstOrDefault(node => node.Name.LocalName == "NotificationSignature")?.Value;
            string timeStampStr = doc.Descendants().FirstOrDefault(node => node.Name.LocalName == "Timestamp")?.Value;
            DateTime timeStamp;
            DateTime.TryParse(timeStampStr, out timeStamp);

            string sig = timeStamp.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + Settings.DevId + Settings.AppId + Settings.AuthCert;
            byte[] sigdata = System.Text.Encoding.ASCII.GetBytes(sig);
            MD5 md5 = new MD5CryptoServiceProvider();
            string md5Hash = System.Convert.ToBase64String(md5.ComputeHash(sigdata));
            return (signature == md5Hash && DateTime.Now.Subtract(timeStamp).Duration().TotalMinutes <= 10);
        }

        //required by Asp.Net's IHttpHandler interface
        public bool IsReusable => false;
    }
}