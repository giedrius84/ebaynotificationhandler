﻿using System.Configuration;

namespace EbayItemRevised
{
    public static class Settings
    {
        public static readonly string ApiUrl = ConfigurationManager.AppSettings["apiUrl"];
        public static readonly string eBayAuthToken = ConfigurationManager.AppSettings["ebayAuthToken"];

        public static readonly string AppId = ConfigurationManager.AppSettings["appId"];
        public static readonly string DevId = ConfigurationManager.AppSettings["devId"];
        public static readonly string AuthCert = ConfigurationManager.AppSettings["certId"];
        public static readonly string SiteId = ConfigurationManager.AppSettings["X-EBAY-API-SITEID"];
        public static readonly string ApiCompatibilityLevel = ConfigurationManager.AppSettings["X-API-COMPATIBILITY-LEVEL"];

        public static readonly string NotificationFolder = ConfigurationManager.AppSettings["notificationFolder"];
    }
}