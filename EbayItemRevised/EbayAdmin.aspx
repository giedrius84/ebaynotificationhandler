﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EbayAdmin.aspx.cs" Inherits="EbayItemRevised.EbayAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>eBay API Admin</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>eBay API Admin</h1>
        <div>
            <asp:Button runat="server" OnClick="OnSetPreferencesClick" Text="Set Notification Preferences" ID="btnSetPreferences" /> &nbsp;
            Result: <asp:Label runat="server" ID="lblPreferencesResult"></asp:Label>
            <br />
        </div>
        <div>
            <h3>Results for ItemRevised notification</h3>
            <asp:Table runat="server" ID="notificationTable" BorderStyle="Solid" GridLines="Both">
                <Rows>
                    <asp:TableHeaderRow runat="server">
                        <Cells>
                            <asp:TableCell runat="server">Date received</asp:TableCell>
                            <asp:TableCell runat="server">Item Id</asp:TableCell>
                            <asp:TableCell runat="server">File name</asp:TableCell>
                        </Cells>
                    </asp:TableHeaderRow>
                </Rows>
            </asp:Table>
        </div>
    </form>
</body>
</html>
