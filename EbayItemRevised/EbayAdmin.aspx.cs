﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace EbayItemRevised
{
    public partial class EbayAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DirectoryInfo directory = new DirectoryInfo(HttpContext.Current.Server.MapPath($"{Settings.NotificationFolder}"));
            if (!directory.Exists) return;
            
            var fileInfo = directory.GetFiles("*.xml").
                OrderByDescending(f => f.CreationTime).
                Select(f => new { Name = f.Name, Date = f.CreationTime });

            foreach (var file in fileInfo)
            {
                var itemId = GetItemId(file.Name);

                var row = CreateTableRowWithFileInfo(file.Name, file.Date, itemId);

                notificationTable.Rows.Add(row);
            }
        }

        private static TableRow CreateTableRowWithFileInfo(string fileName, DateTime fileDate, string itemId)
        {
            TableRow row = new TableRow();
            row.Cells.Add(new TableCell {Text = fileDate.ToString("F", CultureInfo.InvariantCulture) });
            row.Cells.Add(new TableCell {Text = itemId});
            TableCell downloadCell = new TableCell();
            HyperLink hl = new HyperLink
            {
                Text = fileName,
                NavigateUrl = $"{Settings.NotificationFolder}/{fileName}",
            };
            downloadCell.Controls.Add(hl);
            row.Cells.Add(downloadCell);
            return row;
        }

        private static string GetItemId(string name)
        {
            int itemIdStartIndex = name.LastIndexOf("-");
            int itemIdLength = name.LastIndexOf(".") - itemIdStartIndex;
            string itemId = name.Substring(itemIdStartIndex + 1, itemIdLength - 1);
            return itemId;
        }

        protected void OnSetPreferencesClick(object sender, EventArgs e)
        {
            try
            {
                bool result = EbayHttpClient.SetNotificationPreferences();
                lblPreferencesResult.Text = result ? "Success" : "Failure";
                lblPreferencesResult.ForeColor = result ? Color.Green : Color.Red;
            }
            catch (Exception ex)
            {
                lblPreferencesResult.Text = ex.Message;
                lblPreferencesResult.ForeColor = Color.Red;
            }
        }
    }
}