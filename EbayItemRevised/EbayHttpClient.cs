﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace EbayItemRevised
{
    public class EbayHttpClient
    {
        static readonly string SetNotificationPreferencesFileName = HttpContext.Current.Server.MapPath("SetNotificationPreferences.xml");
        static readonly string SetNotificationPreferencesMethod = "SetNotificationPreferences";

        public static bool SetNotificationPreferences()
        {
            string requestXml = CreateRequestContent(SetNotificationPreferencesFileName);
            HttpRequestMessage requestMessage = CreatePostHttpRequestWithContent(SetNotificationPreferencesMethod, requestXml);

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage result = client.SendAsync(requestMessage).Result;
                return result.IsSuccessStatusCode;
            }
        }

        private static string CreateRequestContent(string xmlFileName)
        {
            string contents = File.ReadAllText(xmlFileName);
            contents = contents.Replace("{token}", Settings.eBayAuthToken);
            return contents;
        }

        private static HttpRequestMessage CreatePostHttpRequestWithContent(string remoteOperation, string content)
        {
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, Settings.ApiUrl)
            {
                Content = new StringContent(content)
            };

            message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            message.Headers.Add("X-EBAY-API-SITEID", Settings.SiteId);
            message.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL", Settings.ApiCompatibilityLevel);
            message.Headers.Add("X-EBAY-API-CALL-NAME", remoteOperation);

            return message;
        }
    }
}